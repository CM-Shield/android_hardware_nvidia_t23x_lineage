DTC_FLAGS += -@

old-dtb := $(dtb-y)
old-dtbo := $(dtbo-y)
dtb-y :=
dtbo-y :=
makefile-path := t23x/lineage

dtb-y += tegra234-p3701-0000-p3737-0000.dtb
dtb-y += tegra234-p3701-0004-p3737-0000.dtb
dtb-y += tegra234-p3767-0000-p3768-0000-a0-android.dtb
dtb-y += tegra234-p3767-0001-p3768-0000-a0-android.dtb
dtb-y += tegra234-p3767-0003-p3768-0000-a0-android.dtb
dtb-y += tegra234-p3767-0004-p3768-0000-a0-android.dtb

dtbo-y += tegra234-p3701-overlay.dtbo
dtbo-y += tegra234-p3737-overlay.dtbo
dtbo-y += tegra234-p3767-overlay.dtbo
dtbo-y += tegra234-p3737-audio-codec-rt5658-40pin.dtbo

ifneq ($(dtb-y),)
dtb-y := $(addprefix $(makefile-path)/,$(dtb-y))
endif
ifneq ($(dtbo-y),)
dtbo-y := $(addprefix $(makefile-path)/,$(dtbo-y))
endif

dtb-y += $(old-dtb)
dtbo-y += $(old-dtbo)
